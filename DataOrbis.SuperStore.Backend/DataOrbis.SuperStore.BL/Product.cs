﻿using System.ComponentModel.DataAnnotations;

namespace DataOrbis.SuperStore.BL
{
    public class Product
    {
        [Key]
        public int ProductsID { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescriptionOriginal { get; set; }
        public string ProductDescription { get; set; }
        public string ProductCategory { get; set; }
        public string ProductStatus { get; set; }
        public string ProductBarcode { get; set; }
        public string Rowchecksum { get; set; }

    }
}