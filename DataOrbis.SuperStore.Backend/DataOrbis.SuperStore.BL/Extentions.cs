﻿using DataOrbis.SuperStore.BL;

namespace DataOrbis.SuperStore.Entities
{
    public static class Extentions
    {
        public static void Map(this Product dbProduct, Product product)
        {
            dbProduct.ProductsID = product.ProductsID;
            dbProduct.ProductStatus = product.ProductStatus;
            dbProduct.Rowchecksum = product.Rowchecksum;
            dbProduct.ProductBarcode = product.ProductBarcode;
            dbProduct.ProductCategory = product.ProductCategory;
            dbProduct.ProductCode = product.ProductCode;
            dbProduct.ProductDescription = product.ProductDescription;
            dbProduct.ProductDescriptionOriginal = product.ProductDescriptionOriginal;
        }

        public static bool IsObjectNull(this Product product)
        {
            return product == null;
        }

        public static bool IsEmptyObject(this Product  product)
        {
            return product.ProductsID.Equals(0);
        }
    }
}
