﻿using DataOrbis.SuperStore.BL;
using DataOrbis.SuperStore.Com;
using DataOrbis.SuperStore.EF.Data.Repositories;
using DataOrbis.SuperStore.Entities;
using System.Linq;

namespace DataOrbis.SuperStore.DAL.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
		public ProductRepository(ApplicationDbContext repositoryContext)
			: base(repositoryContext)
		{
		}

		public PagedList<Product> GetProducts(ProductParameters ownerParameters)
		{
			return PagedList<Product>.ToPagedList(FindAll(),
				ownerParameters.PageNumber,
				ownerParameters.PageSize);
		}

		public Product GetProductById(int productId)
		{
			return FindByCondition(owner => owner.ProductsID.Equals(productId))
				.FirstOrDefault();
		}

		public void CreateProduct(Product product)
		{
			Create(product);
		}

		public void UpdateProduct(Product dbProduct, Product product)
		{
			dbProduct.Map(product);
			Update(dbProduct);
		}

		public void DeleteProduct(Product product)
		{
			Delete(product);
		}
	}
}
