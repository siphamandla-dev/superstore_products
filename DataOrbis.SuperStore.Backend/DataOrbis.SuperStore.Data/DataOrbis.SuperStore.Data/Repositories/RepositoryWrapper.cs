﻿using DataOrbis.SuperStore.Com;

namespace DataOrbis.SuperStore.DAL.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private readonly ApplicationDbContext _repoContext;
        private ProductRepository _product;


        public RepositoryWrapper(ApplicationDbContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }


        public IProductRepository Product
        {
            get
            {
                if (_product == null)
                {
                    _product = new ProductRepository(_repoContext);
                }

                return _product;
            }
        }

        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
