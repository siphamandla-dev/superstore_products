﻿using DataOrbis.SuperStore.BL;
using Microsoft.EntityFrameworkCore;

namespace DataOrbis.SuperStore.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }

    }
}
