﻿using DataOrbis.SuperStore.BL;

namespace DataOrbis.SuperStore.Com
{
	public interface IProductRepository : IRepositoryBase<Product>
	{
		PagedList<Product> GetProducts(ProductParameters ownerParameters);
		Product GetProductById(int productId);
		void CreateProduct(Product product);
		void UpdateProduct(Product dbProduct, Product owner);
		void DeleteProduct(Product owner);
	}
}
