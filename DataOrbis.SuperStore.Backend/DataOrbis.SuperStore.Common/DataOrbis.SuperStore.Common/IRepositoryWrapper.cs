﻿namespace DataOrbis.SuperStore.Com
{
    public interface IRepositoryWrapper
    {
        IProductRepository Product { get; }

        void Save();
    }
}
