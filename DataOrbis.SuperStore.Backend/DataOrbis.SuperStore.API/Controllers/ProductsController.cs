﻿using DataOrbis.SuperStore.BL;
using DataOrbis.SuperStore.Com;
using DataOrbis.SuperStore.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DataOrbis.SuperStore.API4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IRepositoryWrapper _repository;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(IRepositoryWrapper repository, ILogger<ProductsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Product> GetProducts([FromQuery] ProductParameters productParameters)
        {
            var products = _repository.Product.GetProducts(productParameters);

            var metadata = new
            {
                products.TotalCount,
                products.PageSize,
                products.CurrentPage,
                products.TotalPages,
                products.HasNext,
                products.HasPrevious
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            if (products.Any())
            {
                _logger.LogInformation("Products recieved form the data source");
            }
            else
            {
                _logger.LogInformation("No Products recieved form the data source");
            }

            return products;
        }

        [HttpGet("{id}", Name = "ProductById")]
        public IActionResult GetProductById(int id)
        {
            var product = _repository.Product.GetProductById(id);

            if (product.IsObjectNull())
            {
                _logger.LogError($"Product with id: {id}, not found in the data source.");
                return NotFound();
            }
            else
            {
                _logger.LogInformation($"Product with id: {id}, found in the data source.");
                return Ok(product);
            }
        }

        [HttpPost]
        public IActionResult CreateProduct([FromBody] Product product)
        {
            if (product.IsObjectNull())
            {
                _logger.LogError("Product object sent from data source is null.");
                return Accepted();
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid product object sent from data source.");
            }

            _repository.Product.CreateProduct(product);
            _repository.Save();

            return Accepted(product);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, [FromBody] Product product)
        {
            if (product.IsObjectNull())
            {
                _logger.LogError("Product object sent from data source is null.");
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid product object sent from data source.");
                return BadRequest();
            }

            var dbProduct = _repository.Product.GetProductById(id);
            if (dbProduct.IsEmptyObject())
            {
                _logger.LogWarning($"Product with id: {id}, found in the data source.");
            }

            _repository.Product.UpdateProduct(dbProduct, product);
            _repository.Save();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var product = _repository.Product.GetProductById(id);
            if (product.IsObjectNull())
            {
                _logger.LogError($"Product with id: {id}, not found in db.");
                return BadRequest();

            }

            _repository.Product.DeleteProduct(product);
            _repository.Save();
            return Ok();
        }
    }
}
