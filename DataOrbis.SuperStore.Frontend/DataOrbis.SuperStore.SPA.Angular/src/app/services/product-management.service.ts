import { Injectable } from '@angular/core';
import Products from '../../assets/Products.json';
import { Product } from '../implementations/product';

@Injectable({
  providedIn: 'root'
})
export class ProductManagementService {
  constructor() { }

  private mock_products: Product[] = Products;

  public getProducts() { return this.mock_products}
  public deleteProduct(product: Product) 
  { 
    var index =  this.mock_products.indexOf(product);
    this.mock_products.splice(index, 1); 
  }
}
