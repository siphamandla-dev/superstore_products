export declare class Product {
    ProductsID : number; 
    ProductCode: number; 
    ProductDescriptionOriginal: string;
    ProductDescription: string;
    ProductCategory: string;
    ProductStatus: string;
    ProductBarcode: string;
}
