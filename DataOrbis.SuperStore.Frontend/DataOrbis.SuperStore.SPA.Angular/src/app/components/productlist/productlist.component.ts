import { Component, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/implementations/product';
import { ProductManagementService } from 'src/app/services/product-management.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
