import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/implementations/product';
import { ProductManagementService } from 'src/app/services/product-management.service';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  constructor(private productService: ProductManagementService) { }
 
  products$: Product[] = [];
  ngOnInit(): void {
    this.products$ = this.productService.getProducts();
  }

  public deleteProduct(product: Product)
  {
     this.productService.deleteProduct(product);
     window.alert('Product deleted from the product list');
  }
}
